package com.example.marvelcharacterlist.core.retrofit

import com.example.marvelcharacterlist.model.CreatorDataWrapper
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface MarvelService {

    @GET("creators")
    fun getCreators(
        @Query("firstName") firstName: String?,
        @Query("limit") limit: Int?,
        @Query("offset") offset: Int?,
        @Query("comics") comics: String?
    ): Single<CreatorDataWrapper>
}