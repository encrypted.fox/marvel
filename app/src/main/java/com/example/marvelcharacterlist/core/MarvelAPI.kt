package com.example.marvelcharacterlist.core

import com.example.marvelcharacterlist.model.CreatorDataWrapper
import io.reactivex.Single

interface MarvelAPI {

    fun getCreators(
        firstName: String? = null,
        limit: Int? = 10,
        offset: Int? = null,
        comics: String? = null
    ): Single<CreatorDataWrapper>


}