package com.example.marvelcharacterlist.core

import com.example.marvelcharacterlist.core.retrofit.MarvelService
import com.example.marvelcharacterlist.model.CreatorDataWrapper
import io.reactivex.Single

class MarvelAPIImpl(val service: MarvelService) :
    MarvelAPI {

    private fun stringifyIntegerList(list: List<Int>?): String? {
        return list?.let { it.fold("", { acc, n -> "$acc,$n" }) }
    }

    override fun getCreators(
        firstName: String?,
        limit: Int?,
        offset: Int?,
        comics: String?
    ): Single<CreatorDataWrapper> {

        return service.getCreators(
            firstName,
            limit,
            offset,
            comics
        )
    }
}