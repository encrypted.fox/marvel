package com.example.marvelcharacterlist.model

import android.os.Parcel
import android.os.Parcelable
import java.util.*

data class Creator(
    val id: Int?,
    val firstName: String?,
    val resourceURI: String?,
    val comics: ComicsList?,
    val urls: List<Url>?,
    val thumbnail: Image?
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readValue(Int::class.java.classLoader) as? Int,
        parcel.readString(),
        parcel.readString(),
        parcel.readParcelable(ComicsList::class.java.classLoader),
        parcel.createTypedArrayList(Url),
        parcel.readParcelable(Image::class.java.classLoader)
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeValue(id)
        parcel.writeString(firstName)
        parcel.writeString(resourceURI)
        parcel.writeParcelable(comics, flags)
        parcel.writeTypedList(urls)
        parcel.writeParcelable(thumbnail, flags)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Creator> {
        override fun createFromParcel(parcel: Parcel): Creator {
            return Creator(parcel)
        }

        override fun newArray(size: Int): Array<Creator?> {
            return arrayOfNulls(size)
        }
    }
}
