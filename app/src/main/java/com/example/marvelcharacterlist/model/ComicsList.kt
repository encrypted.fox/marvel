package com.example.marvelcharacterlist.model

import android.os.Parcel
import android.os.Parcelable

data class ComicsList(
    val available: Int?,
    val returned: Int?,
    val collectionURI: String?,
    val items: List<ComicsSummary>?
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readValue(Int::class.java.classLoader) as? Int,
        parcel.readValue(Int::class.java.classLoader) as? Int,
        parcel.readString(),
        parcel.createTypedArrayList(ComicsSummary)
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeValue(available)
        parcel.writeValue(returned)
        parcel.writeString(collectionURI)
        parcel.writeTypedList(items)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ComicsList> {
        override fun createFromParcel(parcel: Parcel): ComicsList {
            return ComicsList(parcel)
        }

        override fun newArray(size: Int): Array<ComicsList?> {
            return arrayOfNulls(size)
        }
    }
}