package com.example.marvelcharacterlist.model

import android.os.Parcel
import android.os.Parcelable

data class CreatorDataContainer(
    val offset: Int?,
    val limit: Int?,
    val total: Int?,
    val count: Int?,
    val results: List<Creator>?
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readValue(Int::class.java.classLoader) as? Int,
        parcel.readValue(Int::class.java.classLoader) as? Int,
        parcel.readValue(Int::class.java.classLoader) as? Int,
        parcel.readValue(Int::class.java.classLoader) as? Int,
        parcel.createTypedArrayList(Creator)
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeValue(offset)
        parcel.writeValue(limit)
        parcel.writeValue(total)
        parcel.writeValue(count)
        parcel.writeTypedList(results)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<CreatorDataContainer> {
        override fun createFromParcel(parcel: Parcel): CreatorDataContainer {
            return CreatorDataContainer(parcel)
        }

        override fun newArray(size: Int): Array<CreatorDataContainer?> {
            return arrayOfNulls(size)
        }
    }
}