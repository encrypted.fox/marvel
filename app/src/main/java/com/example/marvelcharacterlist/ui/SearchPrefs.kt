package com.example.marvelcharacterlist.ui

import android.content.Context
import android.content.SharedPreferences

class SearchPrefs(context: Context) {
    private val prefsFilename = "settings"
    private var authOk = "false"
    val prefs: SharedPreferences = context.getSharedPreferences(prefsFilename, 0)

    var auth: String
        get() = prefs.getString(authOk, "")
        set(value) = prefs.edit().putString(authOk, value).apply()
}
