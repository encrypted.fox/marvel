package com.example.marvelcharacterlist.ui.viewmodel.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.example.marvelcharacterlist.R
import com.example.marvelcharacterlist.model.Creator
import com.example.marvelcharacterlist.databinding.LayoutCreatorItemBinding
import com.example.marvelcharacterlist.ui.CreatorDetailActivity

class CreatorListAdapter : RecyclerView.Adapter<CreatorListAdapter.ViewHolder>() {

    var items = mutableListOf<Creator?>()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)

        when (viewType) {
            VIEW_ITEM -> {
                val binding = DataBindingUtil.inflate<LayoutCreatorItemBinding>(
                    inflater,
                    R.layout.layout_creator_item,
                    parent,
                    false
                )

                return CreatorViewHolder(binding)
            }

            else -> {  // VIEW_PROGRESS_BAR
                return ViewHolder(inflater.inflate(R.layout.layout_progress_bar, parent, false))
            }
        }
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        items[position]?.let { (holder as? CreatorViewHolder)?.bind(it) }
    }

    override fun getItemViewType(position: Int): Int {
        return if (position == items.lastIndex) VIEW_PROGRESS_BAR else VIEW_ITEM
    }

    open class ViewHolder(view: View) : RecyclerView.ViewHolder(view)

    class CreatorViewHolder(private val binding: LayoutCreatorItemBinding) : ViewHolder(binding.root) {

        fun bind(char: Creator) {
            binding.charName = char.firstName
            val path = "${char.thumbnail?.path}.${char.thumbnail?.extension}"

            Glide.with(binding.root.context)
                .load(path)
                .apply(RequestOptions.circleCropTransform())
                .into(binding.ivCharThumbnail)

            binding.root.setOnClickListener { CreatorDetailActivity.launch(binding.root.context, char) }
        }

    }


    companion object {

        private const val VIEW_ITEM = 0
        private const val VIEW_PROGRESS_BAR = 1

    }
}