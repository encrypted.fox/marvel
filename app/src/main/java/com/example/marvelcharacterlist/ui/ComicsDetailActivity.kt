package com.example.marvelcharacterlist.ui

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.example.marvelcharacterlist.R
import com.example.marvelcharacterlist.databinding.ActivityComicsDetailBinding
import com.example.marvelcharacterlist.model.Creator
import kotlinx.android.synthetic.main.activity_comics_detail.*


class ComicsDetailActivity : AppCompatActivity() {

    private lateinit var binding: ActivityComicsDetailBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_comics_detail)
        val textText = intent.getStringExtra("char")
        text.setText(textText)
    }

    companion object {
        fun launch(context: Context, char: String) {
            val intent = Intent(context, ComicsDetailActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_SINGLE_TOP
            intent.putExtra("char", char)

            context.startActivity(intent)
        }
    }

}
