package com.example.marvelcharacterlist.ui

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.example.marvelcharacterlist.R
import kotlinx.android.synthetic.main.activity_auth.*

class Auth : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_auth)

        val pref = SearchPrefs(this)

        if (pref.auth == "ok") {
            nextActivity()
        }

        button.setOnClickListener {
            val password = editText.text.toString().toIntOrNull()

            if (password == 123) {
                pref.auth = "ok"
                nextActivity()
            } else {
                Toast.makeText(this, "Вы ввели неверный пароль", Toast.LENGTH_LONG).show()
                editText.setText("")
            }
        }
        ebout.setOnClickListener {
            val intent = Intent(this, Ebout::class.java)
            startActivity(intent)
        }
    }

    private fun nextActivity() {
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
    }
}
