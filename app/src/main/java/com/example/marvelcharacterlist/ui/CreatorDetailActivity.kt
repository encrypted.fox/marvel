package com.example.marvelcharacterlist.ui

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.marvelcharacterlist.R
import com.example.marvelcharacterlist.databinding.ActivityCreatorDetailBinding
import com.example.marvelcharacterlist.model.Creator
import com.example.marvelcharacterlist.ui.viewmodel.adapter.SummaryAdapter

class CreatorDetailActivity : AppCompatActivity() {

    private lateinit var binding: ActivityCreatorDetailBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_creator_detail)

        val creator = intent.getParcelableExtra<Creator>(CREATOR)

        binding.charName = creator?.firstName

        binding.tvNoComics.visibility = if (creator.comics?.items?.isNotEmpty() == true) View.GONE else View.VISIBLE

        binding.rvComics.layoutManager = LinearLayoutManager(this)
        binding.rvComics.adapter = SummaryAdapter(creator.comics?.items?.map {it.name.orEmpty() } ?: emptyList())
        binding.rvComics.addItemDecoration(DividerItemDecoration(this, DividerItemDecoration.VERTICAL))

        // Setup navigation bar back arrow.
        binding.toolbar.setNavigationIcon(R.drawable.ic_arrow_back)
        binding.toolbar.setNavigationOnClickListener { finish() }

    }

    companion object {

        private const val CREATOR = "creator"

        fun launch(context: Context, creator: Creator) {
            val intent = Intent(context, CreatorDetailActivity::class.java)
            intent.putExtra(CREATOR, creator)
            intent.flags = Intent.FLAG_ACTIVITY_SINGLE_TOP

            context.startActivity(intent)
        }
    }
}